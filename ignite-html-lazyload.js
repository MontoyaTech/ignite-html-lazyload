import { IgniteProperty } from "../ignite-html/ignite-html.js";
import { IgniteTemplate } from "../ignite-html/ignite-template.js";

/**
 * The helper class that actually watches element.
 * This uses one global intersection observer that is automatically destroyed
 * when it's finished to speed up lazy loading and reduce overhead.
 */
class IgniteLazyLoad {
    static watch(element, callback) {
        //Setup the watching list if we haven't yet.
        if (!IgniteLazyLoad.watching) {
            IgniteLazyLoad.watching = [];
        }

        //Setup the observer if we haven't yet.
        if (!IgniteLazyLoad.observer) {
            IgniteLazyLoad.observer = new IntersectionObserver(results => {
                results.forEach(result => {
                    if (result.isIntersecting) {
                        //Find the watching element.
                        for (var i = 0; i < IgniteLazyLoad.watching.length; i++) {
                            if (IgniteLazyLoad.watching[i].element == result.target) {
                                IgniteLazyLoad.watching[i].callback();
                                IgniteLazyLoad.watching.splice(i, 1);
                                break;
                            }
                        }

                        //Unobserve this element regardless if we were watching it or not.
                        IgniteLazyLoad.observer.unobserve(result.target);
                    }
                });

                //If we have nothing left to watch, stop watching and cleanup.
                if (IgniteLazyLoad.watching.length == 0) {
                    IgniteLazyLoad.observer.disconnect();
                    IgniteLazyLoad.observer = null;
                }
            }, { root: null, trackVisibility: true, delay: 1000 });
        }

        //Add our element to the watching list.
        IgniteLazyLoad.watching.push({ element: element, callback: callback });

        //Observe this element.
        IgniteLazyLoad.observer.observe(element);
    }
}

/**
 * Lazy loads an image in the src attribute for this element.
 * @param {string|Function|IgniteProperty} placeholder The palceholder image to use before the image is loaded. If null, a default one will be used.
 * @param {Function|IgniteProperty} callback The callback function to invoke once the image has been lazy loaded in. By default null.
 * @returns {IgniteTemplate} This ignite template.
 */
 IgniteTemplate.prototype.lazy = function(placeholder = null, callback = null) {
        //Save the original source.
        this._lazy_src = this._attributes["src"];

        //If lazy src is invalid, use a transparent placeholder.
        if (!this._lazy_src) {
            this._lazy_src = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=";
        }

        //Override the attribute changed function, if someone tries to set src and our lazy_src is set, override that.
        //Otherwise we might show the wrong lazy loaded image.
        var originalAttributeChanged = this.onAttributeChanged;
        this.onAttributeChanged = (name, newValue) => {
            if (name == "src" && this._lazy_src) {
                //Update the lazy src
                this._lazy_src = newValue;
            } else {
                //Call the original function.
                originalAttributeChanged.call(this, name, newValue);
            }
        };

        //If we have a placeholder set it
        if (placeholder) {
            if (placeholder instanceof IgniteProperty) {
                this._attributes["src"] = placeholder.value;
            } else if (placeholder instanceof Function) {
                this._attributes["src"] = placeholder();
            } else {
                this._attributes["src"] = placeholder;
            }
        } else {
            //Use default transparent empty png if no placeholder.
            this._attributes["src"] = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=";
        }

        //Add a constructor to add our element to the lazy load watcher.
        this._constructors.push(() => {
            IgniteLazyLoad.watch(this.element, () => {
                if (this._lazy_src) {
                    this._attributes["src"] = this._lazy_src;
                    this.element.setAttribute("src", this._lazy_src);
                    this._lazy_src = null;
    
                    //If we have a callback invoke it.
                    if (callback instanceof IgniteProperty) {
                        callback.value();
                    } else if (callback instanceof Function) {
                        callback();
                    }
                }
            });
        });

    return this;
}